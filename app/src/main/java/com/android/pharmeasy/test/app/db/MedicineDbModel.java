package com.android.pharmeasy.test.app.db;

/**
 * Description Please
 *
 * @author pranit
 * @version 1.0
 * @since 31/7/16
 */

public class MedicineDbModel {
    long id;
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
