package com.android.pharmeasy.test.app.network;

import com.android.pharmeasy.test.app.model.MedicineModel;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Description Please
 *
 * @author pranit
 * @version 1.0
 * @since 9/8/16
 */

public class RetrofitFactory {
    //https://www.1mg.com/api/v1/search/autocomplete?name=b&pageSize=10000000&_=1435404923427
    public static Retrofit get() {
        return new Retrofit.Builder()
                .baseUrl("https://www.1mg.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    // end point
    public static interface SearchEndPoint{
        @GET("search/autocomplete")
        Call<MedicineModel> search(@Query("name") String name, @Query("pageSize") long pageSize, @Query("_") long timestamp);
    }
}
