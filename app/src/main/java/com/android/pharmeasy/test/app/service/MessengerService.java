package com.android.pharmeasy.test.app.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.android.pharmeasy.test.app.db.DataSource;
import com.android.pharmeasy.test.app.model.MedicineModel;
import com.android.pharmeasy.test.app.model.Result;
import com.android.pharmeasy.test.app.network.RetrofitFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessengerService extends Service implements Callback<MedicineModel> {
    /**
     * Command to the service to display a message
     */
    public static final int MSG_SAY_HELLO = 1;
    private static final String TAG = MessengerService.class.getSimpleName();

    private RetrofitFactory.SearchEndPoint mSearchEndPoint;

    private Call<MedicineModel> mCall;

    private Messenger mActivityMessanger;

    private DataSource mDataSource;


    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SAY_HELLO:
                    Toast.makeText(getApplicationContext(), "Fetching medicine list...", Toast.LENGTH_SHORT).show();
                    mCall = mSearchEndPoint.search("p", 50, System.currentTimeMillis());
                    mCall.enqueue(MessengerService.this);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind: ");
        mDataSource = new DataSource(this);
        mDataSource.open();
        mDataSource.deleteAll();
        mActivityMessanger = (Messenger) intent.getExtras().get("MSG");
        mSearchEndPoint = RetrofitFactory.get().create(RetrofitFactory.SearchEndPoint.class);
        return mMessenger.getBinder();
    }

    @Override
    public void onResponse(Call<MedicineModel> call, Response<MedicineModel> response) {

        MedicineModel model = response.body();
        if (model != null) {
            for(Result result: model.getResult()) {
                mDataSource.insert(result.getName());
            }
            sayHello();
        }
    }

    public void sayHello() {
        if (mActivityMessanger == null) return;
        // Create and send a message to the service, using a supported 'what' value
        Message msg = Message.obtain(null, MSG_SAY_HELLO, 0, 0);
        try {
            mActivityMessanger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<MedicineModel> call, Throwable t) {

    }

    @Override
    public boolean onUnbind(Intent intent) {
        mDataSource.close();
        Log.i(TAG, "onUnbind: ");
        if (mCall != null && !mCall.isCanceled()) {
            mCall.cancel();
        }
        return super.onUnbind(intent);
    }
}