package com.android.pharmeasy.test.app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
   private static final String DATABASE_NAME = "medicine.db";


   public static final String TABLE_MEDICINE = "MEDICINE";

   public static final String COLUMN_ID = "ID";
   public static final String COLUMN_NAME = "NAME";

   private static final String CREATE_DAY_PARENT_TABLE = "create table "+ TABLE_MEDICINE +" ( "+COLUMN_ID+" integer primary key autoincrement, "+COLUMN_NAME+" text not null);";

   private static final int DATABASE_VERSION = 1;

   public DBHelper(Context context){
      super(context,DATABASE_NAME,null,DATABASE_VERSION);
   }
   public void onCreate(SQLiteDatabase db) {
      db.execSQL(CREATE_DAY_PARENT_TABLE);
   }
   public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {}
}