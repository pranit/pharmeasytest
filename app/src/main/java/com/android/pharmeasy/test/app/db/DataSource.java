package com.android.pharmeasy.test.app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Description Please
 *
 * @author pranit
 * @version 1.0
 * @since 31/7/16
 */

public class DataSource {

    final DBHelper dbHelper;
    SQLiteDatabase database;
    String[] dayParentColumns = {DBHelper.COLUMN_ID, DBHelper.COLUMN_NAME};

    public DataSource(Context context) {
        this.dbHelper = new DBHelper(context);
    }

    public void open(){
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public long insert(String name){
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_NAME,name);
        return database.insert(DBHelper.TABLE_MEDICINE,null,values);
    }

    public List<MedicineDbModel> getAllMedicines(){
        List<MedicineDbModel> models = new ArrayList<>();
        Cursor cursor = database.query(DBHelper.TABLE_MEDICINE,
                dayParentColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            MedicineDbModel comment = cursorToComment(cursor);
            models.add(comment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return models;
    }

    public void deleteAll(){
        database.delete(DBHelper.TABLE_MEDICINE,null,null);
    }

    private MedicineDbModel cursorToComment(Cursor cursor) {
        MedicineDbModel comment = new MedicineDbModel();
        comment.setId(cursor.getLong(0));
        comment.setName(cursor.getString(1));
        return comment;
    }
}
