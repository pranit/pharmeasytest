package com.android.pharmeasy.test.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

@SerializedName("su")
@Expose
private Integer su;
@SerializedName("mrp")
@Expose
private Double mrp;
@SerializedName("discountPerc")
@Expose
private Integer discountPerc;
@SerializedName("mfId")
@Expose
private Integer mfId;
@SerializedName("type")
@Expose
private String type;
@SerializedName("uip")
@Expose
private Integer uip;
@SerializedName("drug_form")
@Expose
private String drugForm;
@SerializedName("productsForBrand")
@Expose
private Object productsForBrand;
@SerializedName("meta")
@Expose
private String meta;
@SerializedName("slug")
@Expose
private String slug;
@SerializedName("packSize")
@Expose
private String packSize;
@SerializedName("pForm")
@Expose
private String pForm;
@SerializedName("hkpDrugCode")
@Expose
private Integer hkpDrugCode;
@SerializedName("oPrice")
@Expose
private Double oPrice;
@SerializedName("generics")
@Expose
private Object generics;
@SerializedName("prescriptionRequired")
@Expose
private Boolean prescriptionRequired;
@SerializedName("imgUrl")
@Expose
private Object imgUrl;
@SerializedName("label")
@Expose
private String label;
@SerializedName("therapeuticClassName")
@Expose
private Object therapeuticClassName;
@SerializedName("manufacturer")
@Expose
private String manufacturer;
@SerializedName("uPrice")
@Expose
private Double uPrice;
@SerializedName("available")
@Expose
private Boolean available;
@SerializedName("form")
@Expose
private String form;
@SerializedName("id")
@Expose
private Integer id;
@SerializedName("packForm")
@Expose
private String packForm;
@SerializedName("name")
@Expose
private String name;
@SerializedName("packSizeLabel")
@Expose
private String packSizeLabel;
@SerializedName("mappedPForm")
@Expose
private String mappedPForm;

/**
* 
* @return
* The su
*/
public Integer getSu() {
return su;
}

/**
* 
* @param su
* The su
*/
public void setSu(Integer su) {
this.su = su;
}

/**
* 
* @return
* The mrp
*/
public Double getMrp() {
return mrp;
}

/**
* 
* @param mrp
* The mrp
*/
public void setMrp(Double mrp) {
this.mrp = mrp;
}

/**
* 
* @return
* The discountPerc
*/
public Integer getDiscountPerc() {
return discountPerc;
}

/**
* 
* @param discountPerc
* The discountPerc
*/
public void setDiscountPerc(Integer discountPerc) {
this.discountPerc = discountPerc;
}

/**
* 
* @return
* The mfId
*/
public Integer getMfId() {
return mfId;
}

/**
* 
* @param mfId
* The mfId
*/
public void setMfId(Integer mfId) {
this.mfId = mfId;
}

/**
* 
* @return
* The type
*/
public String getType() {
return type;
}

/**
* 
* @param type
* The type
*/
public void setType(String type) {
this.type = type;
}

/**
* 
* @return
* The uip
*/
public Integer getUip() {
return uip;
}

/**
* 
* @param uip
* The uip
*/
public void setUip(Integer uip) {
this.uip = uip;
}

/**
* 
* @return
* The drugForm
*/
public String getDrugForm() {
return drugForm;
}

/**
* 
* @param drugForm
* The drug_form
*/
public void setDrugForm(String drugForm) {
this.drugForm = drugForm;
}

/**
* 
* @return
* The productsForBrand
*/
public Object getProductsForBrand() {
return productsForBrand;
}

/**
* 
* @param productsForBrand
* The productsForBrand
*/
public void setProductsForBrand(Object productsForBrand) {
this.productsForBrand = productsForBrand;
}

/**
* 
* @return
* The meta
*/
public String getMeta() {
return meta;
}

/**
* 
* @param meta
* The meta
*/
public void setMeta(String meta) {
this.meta = meta;
}

/**
* 
* @return
* The slug
*/
public String getSlug() {
return slug;
}

/**
* 
* @param slug
* The slug
*/
public void setSlug(String slug) {
this.slug = slug;
}

/**
* 
* @return
* The packSize
*/
public String getPackSize() {
return packSize;
}

/**
* 
* @param packSize
* The packSize
*/
public void setPackSize(String packSize) {
this.packSize = packSize;
}

/**
* 
* @return
* The pForm
*/
public String getPForm() {
return pForm;
}

/**
* 
* @param pForm
* The pForm
*/
public void setPForm(String pForm) {
this.pForm = pForm;
}

/**
* 
* @return
* The hkpDrugCode
*/
public Integer getHkpDrugCode() {
return hkpDrugCode;
}

/**
* 
* @param hkpDrugCode
* The hkpDrugCode
*/
public void setHkpDrugCode(Integer hkpDrugCode) {
this.hkpDrugCode = hkpDrugCode;
}

/**
* 
* @return
* The oPrice
*/
public Double getOPrice() {
return oPrice;
}

/**
* 
* @param oPrice
* The oPrice
*/
public void setOPrice(Double oPrice) {
this.oPrice = oPrice;
}

/**
* 
* @return
* The generics
*/
public Object getGenerics() {
return generics;
}

/**
* 
* @param generics
* The generics
*/
public void setGenerics(Object generics) {
this.generics = generics;
}

/**
* 
* @return
* The prescriptionRequired
*/
public Boolean getPrescriptionRequired() {
return prescriptionRequired;
}

/**
* 
* @param prescriptionRequired
* The prescriptionRequired
*/
public void setPrescriptionRequired(Boolean prescriptionRequired) {
this.prescriptionRequired = prescriptionRequired;
}

/**
* 
* @return
* The imgUrl
*/
public Object getImgUrl() {
return imgUrl;
}

/**
* 
* @param imgUrl
* The imgUrl
*/
public void setImgUrl(Object imgUrl) {
this.imgUrl = imgUrl;
}

/**
* 
* @return
* The label
*/
public String getLabel() {
return label;
}

/**
* 
* @param label
* The label
*/
public void setLabel(String label) {
this.label = label;
}

/**
* 
* @return
* The therapeuticClassName
*/
public Object getTherapeuticClassName() {
return therapeuticClassName;
}

/**
* 
* @param therapeuticClassName
* The therapeuticClassName
*/
public void setTherapeuticClassName(Object therapeuticClassName) {
this.therapeuticClassName = therapeuticClassName;
}

/**
* 
* @return
* The manufacturer
*/
public String getManufacturer() {
return manufacturer;
}

/**
* 
* @param manufacturer
* The manufacturer
*/
public void setManufacturer(String manufacturer) {
this.manufacturer = manufacturer;
}

/**
* 
* @return
* The uPrice
*/
public Double getUPrice() {
return uPrice;
}

/**
* 
* @param uPrice
* The uPrice
*/
public void setUPrice(Double uPrice) {
this.uPrice = uPrice;
}

/**
* 
* @return
* The available
*/
public Boolean getAvailable() {
return available;
}

/**
* 
* @param available
* The available
*/
public void setAvailable(Boolean available) {
this.available = available;
}

/**
* 
* @return
* The form
*/
public String getForm() {
return form;
}

/**
* 
* @param form
* The form
*/
public void setForm(String form) {
this.form = form;
}

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The packForm
*/
public String getPackForm() {
return packForm;
}

/**
* 
* @param packForm
* The packForm
*/
public void setPackForm(String packForm) {
this.packForm = packForm;
}

/**
* 
* @return
* The name
*/
public String getName() {
return name;
}

/**
* 
* @param name
* The name
*/
public void setName(String name) {
this.name = name;
}

/**
* 
* @return
* The packSizeLabel
*/
public String getPackSizeLabel() {
return packSizeLabel;
}

/**
* 
* @param packSizeLabel
* The packSizeLabel
*/
public void setPackSizeLabel(String packSizeLabel) {
this.packSizeLabel = packSizeLabel;
}

/**
* 
* @return
* The mappedPForm
*/
public String getMappedPForm() {
return mappedPForm;
}

/**
* 
* @param mappedPForm
* The mappedPForm
*/
public void setMappedPForm(String mappedPForm) {
this.mappedPForm = mappedPForm;
}

}
