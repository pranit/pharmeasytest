package com.android.pharmeasy.test.app.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.android.pharmeasy.test.app.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashActivity.this.startActivity(SlideShowActivity.getStartIntent(SplashActivity.this));
                SplashActivity.this.finish();
            }
        }, 3*1000);
    }
}
