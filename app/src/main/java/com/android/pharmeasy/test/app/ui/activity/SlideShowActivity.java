package com.android.pharmeasy.test.app.ui.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.pharmeasy.test.app.R;
import com.android.pharmeasy.test.app.db.DataSource;
import com.android.pharmeasy.test.app.db.MedicineDbModel;
import com.android.pharmeasy.test.app.service.MessengerService;

import java.util.ArrayList;
import java.util.List;

import static com.android.pharmeasy.test.app.service.MessengerService.MSG_SAY_HELLO;

public class SlideShowActivity extends AppCompatActivity {

    /** Messenger for communicating with the service. */
    Messenger mService = null;

    /** Flag indicating whether we have called bind on the service. */
    boolean mBound;

    RecyclerView mList;

    MedicineAdapter mAdapter;

    List<MedicineDbModel> mMedicines;

    public static Intent getStartIntent(Context context){
        return new Intent(context, SlideShowActivity.class);
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mService = new Messenger(service);
            mBound = true;
            sayHello();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_show);
        mList = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mList.setLayoutManager(horizontalLayoutManagaer);
        mMedicines = new ArrayList<>();
        mAdapter = new MedicineAdapter(mMedicines);
        mList.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MessengerService.class);
        IncomingHandler handler = new IncomingHandler();
        Messenger messenger = new Messenger(handler);
        intent.putExtra("MSG", messenger);

        // Bind to the service
        bindService(intent, mConnection,
                Context.BIND_AUTO_CREATE);
    }

    public void sayHello() {
        if (!mBound) return;
        // Create and send a message to the service, using a supported 'what' value
        Message msg = Message.obtain(null, MSG_SAY_HELLO, 0, 0);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SAY_HELLO:
                    DataSource dataSource = new DataSource(SlideShowActivity.this);
                    mMedicines.clear();
                    dataSource.open();
                    mMedicines.addAll(dataSource.getAllMedicines());
                    mAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "Loading medicine list...", Toast.LENGTH_SHORT).show();
                    dataSource.close();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    public static class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.MyViewHolder> {

        private List<MedicineDbModel> list;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txtName;

            public MyViewHolder(View view) {
                super(view);
                txtName = (TextView) view.findViewById(R.id.txtName);
            }
        }


        public MedicineAdapter(List<MedicineDbModel> horizontalList) {
            this.list = horizontalList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_medicine, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.txtName.setText(list.get(position).getName());
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
